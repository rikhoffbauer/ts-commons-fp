import getKeys from "../../src/object/getKeys";

describe("getKeys(obj)", () => {
    it("should return the keys of the object", () => {
        const obj = { a: 1, b: 2, c: 3 };
        const keys = ["a", "b", "c"];

        expect(getKeys(obj)).toEqual(keys);
    });
});
