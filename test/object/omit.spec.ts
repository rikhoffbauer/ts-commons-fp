import omit from "../../src/object/omit";

describe(`omit(obj)(...keys)`, () => {
    it(`should not mutate the passed in object`, () => {
        const obj = Object.freeze({
            a: "a",
            b: "b",
        });

        // because we have frozen the object we can be sure it hasn't been mutated
        // by testing if it throw an error
        expect(() => omit<typeof obj, any>()(obj)).not.toThrow(
            `Cannot assign to read only property`,
        );
    });

    it(`should remove the provided keys from a shallow clone of the provided object`, async () => {
        const obj = {
            a: "a",
            b: "b",
            c: "c",
        };
        const omittedProperties = Object.keys(obj).slice(
            1,
        ) as (keyof typeof obj)[];
        const keptProperties = (Object.keys(
            obj,
        ) as (keyof typeof obj)[]).filter(
            key => !omittedProperties.includes(key),
        );
        const result = omit<typeof obj, keyof typeof obj>(...omittedProperties)(
            obj,
        );

        omittedProperties.forEach(omittedProperty => {
            expect(result).not.toHaveProperty(omittedProperty);
        });

        keptProperties.forEach(keptProperty => {
            expect(result).toHaveProperty(keptProperty);
        });
    });
});
