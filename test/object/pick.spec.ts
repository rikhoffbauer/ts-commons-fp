import pick from "../../src/object/pick";

describe(`pick(obj)(...keys)`, () => {
    it(`should not mutate the passed in object`, () => {
        const obj = Object.freeze({
            a: "a",
            b: "b",
        });

        // because we have frozen the object we can be sure it hasn't been mutated
        // by testing if it throw an error
        expect(() => pick<typeof obj, any>()(obj)).not.toThrow(
            `Cannot assign to read only property`,
        );
    });

    it(`should return an object with only the picked properties`, async () => {
        const obj = {
            a: "a",
            b: "b",
            c: "c",
        };
        type Key = "a" | "b" | "c";
        const pickedProperties: Key[] = Object.keys(obj).slice(1) as Key[];
        const keptProperties = Object.keys(obj).filter(
            key => !pickedProperties.includes(key as Key),
        );
        const result = pick<typeof obj, keyof typeof obj>(...pickedProperties)(
            obj,
        );

        pickedProperties.forEach(pickedProperty => {
            expect(result).toHaveProperty(pickedProperty);
        });

        keptProperties.forEach(omittedProperty => {
            expect(result).not.toHaveProperty(omittedProperty);
        });
    });
});
