import getValues from "../../src/object/getValues";

describe("getValues(obj)", () => {
    it("should return the keys of the object", () => {
        const obj = { a: 1, b: 2, c: 3 };
        const values = [1, 2, 3];

        expect(getValues(obj)).toEqual(values);
    });
});
