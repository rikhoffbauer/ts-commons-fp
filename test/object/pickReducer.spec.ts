import pickReducer from "../../src/object/_pickReducer";

describe("pickReducer(obj)(obj, key)", () => {
    it("should reduce an array of keys to an object with the picked properties", () => {
        const obj = { a: 1, b: 1 };
        const expected = { a: 1 };
        const result = (["a"] as (keyof typeof obj)[]).reduce(
            pickReducer<typeof obj, keyof typeof obj>(obj),
            {} as typeof obj,
        );

        expect(result).toEqual(expected);
    });
});
