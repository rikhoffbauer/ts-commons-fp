import getProperty from "../../src/object/getProperty";

describe("getKeys(obj)", () => {
    it("should return the keys of the object", () => {
        const obj = { a: 1, b: 2, c: 3 };

        expect(getProperty<typeof obj, keyof typeof obj>("a")(obj)).toEqual(1);
    });
});
