import includes from "../../src/array/includes";

describe("includes(value)(arr)", () => {
    it("should return true if the value is present in the array", () => {
        const arr = [1, 2, 3];

        expect(includes(2)(arr)).toEqual(true);
        expect(includes(4)(arr)).toEqual(false);
    });
});
