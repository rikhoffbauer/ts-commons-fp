import getCenterItem from "../../src/array/getCenterItem";

describe("getCenterIndex(arr)", () => {
    it("should return the center index of the array", () => {
        const center = {};
        const arr = [1, center, 3];

        expect(getCenterItem(arr)).toEqual(center);
    });
    it("should return undefined if there is no center (i.e. an even number length of the array)", () => {
        const arr = [1, 2, 3, 4];

        expect(getCenterItem(arr)).toEqual(undefined);
    });
});
