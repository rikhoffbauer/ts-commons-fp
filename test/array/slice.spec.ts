import slice from "../../src/array/slice";

describe("slice(start, end?)(arr)", () => {
    it("should slice the array using the provided start and end", () => {
        const arr = [1, 2, 3, 4, 5];
        const results = [2, 3];

        expect(slice(1, 3)(arr)).toEqual(results);
    });
});
