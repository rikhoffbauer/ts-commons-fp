import unique from "../../src/array/unique";

describe("unique(arr)", () => {
    it("should return an array with only the unique items from the provided array", () => {
        const arr = [1, 2, 3, 1, 2, 3, 3, 3];
        const results = [1, 2, 3];

        expect(unique(arr)).toEqual(results);
    });
});
