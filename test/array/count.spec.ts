import count from "../../src/array/count";

describe("count(predicate)(value)", () => {
    it("should count the amount of matching in an array using the provided predicate", () => {
        const arr = [1, 2, 3, 4];
        const isEven = (v: number) => v % 2 === 0;

        expect(count(isEven)(arr)).toEqual(2);
    });
});
