import getByIndex from "../../src/array/getByIndex";

describe("getByIndex(index)(arr)", () => {
    it("should return the item at the index of the array", () => {
        const arr = [1, 2, 3];

        expect(getByIndex(0)(arr)).toEqual(1);
        expect(getByIndex(1)(arr)).toEqual(2);
        expect(getByIndex(2)(arr)).toEqual(3);
    });
});
