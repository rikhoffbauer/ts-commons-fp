import countOccurrences from "../../src/array/countOccurrences";

describe("countOccurrences(value)(arr)", () => {
    it("should count the occurrences of the provided value in the array", () => {
        const value = 1;
        const arr = [value, 2, 2, value, value];
        expect(countOccurrences(value)(arr)).toBe(3);
    });
});
