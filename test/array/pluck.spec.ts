import pluck from "../../src/array/pluck";

describe("pluck(property)(array)", () => {
    it("should return an array with the values plucked from the objects in the provided array", () => {
        const arr = [{ a: 1 }, { a: 2 }, { a: 3 }];
        const results = [1, 2, 3];

        expect(pluck<{ a: number }, "a">("a")(arr)).toEqual(results);
    });
});
