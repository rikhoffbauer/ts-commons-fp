import toObject from "../../src/array/toObject";

describe("toObject(idProperty)(arr)", () => {
    it("should convert an array to an object map", () => {
        const arr = [{ id: 1 }, { id: 2 }, { id: 3 }];
        const result = {
            1: arr[0],
            2: arr[1],
            3: arr[2],
        };

        expect(toObject<typeof arr[0], "id">("id")(arr)).toEqual(result);
    });
});
