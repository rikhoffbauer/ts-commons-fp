import sort from "../../src/array/sort";
import { Sorter } from "../../src/types/index";

describe("sort(sorter)(arr)", () => {
    it("should sort the array using the sorter", () => {
        const arr = [1, 2, 3];
        const results = [3, 2, 1];
        const descending: Sorter<number> = (a, b) => b - a;

        expect(sort(descending)(arr)).toEqual(results);
    });
});
