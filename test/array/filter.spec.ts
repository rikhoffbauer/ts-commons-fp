import filter from "../../src/array/filter";

describe("filter(predicate)(value)", () => {
    it("should filter the array using the provided predicate", () => {
        const arr = [1, 2, 3];
        const results = [2];
        const isEven = (v: number) => v % 2 === 0;

        expect(filter(isEven)(arr)).toEqual(results);
    });
});
