import reduce from "../../src/array/reduce";

describe("reduce(reducer, initialValue)(arr)", () => {
    it("should reduce the array using the provided reducer", () => {
        const arr = [1, 2, 3];
        const results = 6;
        const reducer = (total: number = 0, n: number) => total + n;

        expect(reduce(reducer)(arr)).toEqual(results);
    });
});
