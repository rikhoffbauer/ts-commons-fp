import map from "../../src/array/map";

describe("map(transformer)(value)", () => {
    it("should map the array using the provided transformer", () => {
        const arr = [1, 2, 3];
        const results = [2, 3, 4];
        const addOne = (v: number) => v + 1;

        expect(map(addOne)(arr)).toEqual(results);
    });
});
