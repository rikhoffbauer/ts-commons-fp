import includedIn from "../../src/array/includedIn";

describe("includedIn(arr)(value)", () => {
    it("should return true if the value is present in the array", () => {
        const arr = [1, 2, 3];

        expect(includedIn(arr)(2)).toEqual(true);
        expect(includedIn(arr)(4)).toEqual(false);
    });
});
