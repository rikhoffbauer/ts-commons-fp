import getCenterIndex from "../../src/array/getCenterIndex";

describe("getCenterIndex(arr)", () => {
    it("should return the center index of the array", () => {
        const arr = [1, 2, 3];

        expect(getCenterIndex(arr)).toEqual(1);
    });
    it("should return undefined if there is no center (i.e. an even number length of the array)", () => {
        const arr = [1, 2, 3, 4];

        expect(getCenterIndex(arr)).toEqual(undefined);
    });
});
