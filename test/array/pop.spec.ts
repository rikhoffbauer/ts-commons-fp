import pop from "../../src/array/pop";

describe("pop(arr)", () => {
    it("should return the last item in the array", () => {
        const arr = [1, 2, 3];
        const result = 3;

        expect(pop(arr)).toEqual(result);
    });
});
