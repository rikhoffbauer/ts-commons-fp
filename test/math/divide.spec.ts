import divide from "../../src/math/divide";

describe("divide(minuend)(subtrahend)", () => {
    it("should divide the numbers", () => {
        const divideByTen = divide(10);
        expect(divideByTen(25)).toBe(25 / 10);
    });
});
