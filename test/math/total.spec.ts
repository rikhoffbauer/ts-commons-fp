import total from "../../src/math/total";

describe("total(numberArray)", () => {
    it("should return the total of the numbers in the array", () => {
        expect([1, 2, 3].reduce(total)).toBe(6);
        expect([1, 2, 3].reduce(total, 0)).toBe(6);
    });
});
