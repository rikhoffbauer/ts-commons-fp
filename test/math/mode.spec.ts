import mode from "../../src/math/mode";

describe("mode(numberArray)", () => {
    it("should return the mode of the numbers in the array", () => {
        expect(mode([1, 2, 3])).toEqual([]);
        expect(mode([1, 2, 3, 4, 5])).toEqual([]);
        expect(mode([1, 1, 3, 4, 5])).toEqual([1]);
        expect(mode([1, 1, 3, 3, 5])).toEqual([1, 3]);
    });
    it("should return an empty array if no numbers are provided", () => {
        expect(mode([])).toEqual([]);
    });
});
