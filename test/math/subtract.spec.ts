import subtract from "../../src/math/subtract";

describe("subtract(minuend)(subtrahend)", () => {
    it("should subtract the numbers", () => {
        const subtractTen = subtract(10);
        expect(subtract(10, 25)).toBe(25 - 10);
        expect(subtractTen(25)).toBe(25 - 10);
    });
});
