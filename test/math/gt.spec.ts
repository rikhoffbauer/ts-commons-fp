import gt from "../../src/math/gt";

describe("gt(rightOperand)(leftOperand)", () => {
    it("should return true if the number passed in last is less than or equal to the number passed in first", () => {
        const gt10 = gt(10);
        expect(gt10(25)).toBe(25 > 10);
        expect(gt10(10)).toBe(10 > 10);
        expect(gt10(5)).toBe(5 > 10);
    });
});
