import max from "../../src/math/max";

describe("max(numberArray)", () => {
    it("should return the highest value from the array", () => {
        expect(max([1, 2, 3])).toEqual(3);
        expect(max([1, 2, 3, 4, 5])).toEqual(5);
        expect(max([100, 1, 3, 4, 5])).toEqual(100);
    });
    it("should return -Infinity if no numbers are provided", () => {
        expect(max([])).toEqual(-Infinity);
    });
});
