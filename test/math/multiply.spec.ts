import multiply from "../../src/math/multiply";

describe("multiply(minuend)(subtrahend)", () => {
    it("should multiply the numbers", () => {
        const multiplyByTen = multiply(10);
        expect(multiplyByTen(25)).toBe(25 * 10);
    });
});
