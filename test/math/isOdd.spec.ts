import isOdd from "../../src/math/isOdd";

describe("isOdd(number)", () => {
    it("should return true if the number is odd", () => {
        expect(isOdd(3)).toBe(true);
        expect(isOdd(2)).toBe(false);
    });
});
