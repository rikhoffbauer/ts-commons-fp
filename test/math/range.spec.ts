import range from "../../src/math/range";

describe("range(arr)", () => {
    it("should range the numbers", () => {
        expect(range([1, 2, 5])).toBe(4);
    });
});
