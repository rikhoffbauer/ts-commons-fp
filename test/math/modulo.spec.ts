import modulo from "../../src/math/modulo";

describe("modulo(divisor)(dividend)", () => {
    it("should return the modulo of the divisor", () => {
        expect(modulo(3)(9)).toBe(9 % 3);
        expect(modulo(4)(9)).toBe(9 % 4);
    });
});
