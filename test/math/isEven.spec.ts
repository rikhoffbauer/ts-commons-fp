import isEven from "../../src/math/isEven";

describe("isEven(number)", () => {
    it("should return true if the number is even", () => {
        expect(isEven(3)).toBe(false);
        expect(isEven(2)).toBe(true);
    });
});
