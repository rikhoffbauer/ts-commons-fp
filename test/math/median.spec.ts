import median from "../../src/math/median";

describe("median(numberArray)", () => {
    it("should return the the middle item of the (sorted) array", () => {
        expect(median([1, 3, 2])).toBe(2);
    });
    it("should return the mean if the array has an even length", () => {
        expect(median([1, 2, 3, 4])).toBe(2.5);
    });
    it("should return NaN if no numbers are provided", () => {
        expect(median([])).toBe(NaN);
    });
});
