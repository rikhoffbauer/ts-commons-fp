import isNegative from "../../src/math/isNegative";

describe("isNegative(rightOperand)(leftOperand)", () => {
    it("should return true if the number is negative", () => {
        expect(isNegative(1)).toBe(false);
        expect(isNegative(-1)).toBe(true);
        expect(isNegative(0)).toBe(false);
    });
});
