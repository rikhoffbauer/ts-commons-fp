import lte from "../../src/math/lte";

describe("lte(rightOperand)(leftOperand)", () => {
    it("should return true if the number passed in last is less than or equal to the number passed in first", () => {
        const lte10 = lte(10);
        expect(lte10(25)).toBe(25 <= 10);
        expect(lte10(10)).toBe(10 <= 10);
        expect(lte10(5)).toBe(5 <= 10);
    });
});
