import isZero from "../../src/math/isZero";

describe("isZero(rightOperand)(leftOperand)", () => {
    it("should return true if the number is zero", () => {
        expect(isZero(1)).toBe(false);
        expect(isZero(-1)).toBe(false);
        expect(isZero(0)).toBe(true);
    });
});
