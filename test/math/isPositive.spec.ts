import isPositive from "../../src/math/isPositive";

describe("isPositive(rightOperand)(leftOperand)", () => {
    it("should return true if the number is positive", () => {
        expect(isPositive(1)).toBe(true);
        expect(isPositive(-1)).toBe(false);
        expect(isPositive(0)).toBe(false);
    });
});
