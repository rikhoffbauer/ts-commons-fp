import gte from "../../src/math/gte";

describe("gte(rightOperand)(leftOperand)", () => {
    it("should return true if the number passed in last is less than or equal to the number passed in first", () => {
        const gte10 = gte(10);
        expect(gte10(25)).toBe(25 >= 10);
        expect(gte10(10)).toBe(10 >= 10);
        expect(gte10(5)).toBe(5 >= 10);
    });
});
