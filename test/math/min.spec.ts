import min from "../../src/math/min";

describe("min(numberArray)", () => {
    it("should return the highest value from the array", () => {
        expect(min([1, 2, 3])).toEqual(1);
        expect(min([1, 2, 3, 4, 5])).toEqual(1);
        expect(min([100, 1, 3, 4, 5])).toEqual(1);
    });
    it("should return Infinity if no numbers are provided", () => {
        expect(min([])).toEqual(Infinity);
    });
});
