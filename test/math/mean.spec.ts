import mean from "../../src/math/mean";

describe("mean(numberArray)", () => {
    it("should return the mean of the numbers in the array", () => {
        expect(mean([1, 2, 3])).toBe(2);
        expect(mean([-100, 100])).toBe(0);
    });
    it("should return NaN if no numbers are provided", () => {
        expect(mean([])).toBe(NaN);
    });
});
