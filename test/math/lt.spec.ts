import lt from "../../src/math/lt";

describe("lt(rightOperand)(leftOperand)", () => {
    it("should return true if the number passed in last is less than or equal to the number passed in first", () => {
        const lt10 = lt(10);
        expect(lt10(25)).toBe(25 < 10);
        expect(lt10(10)).toBe(10 < 10);
        expect(lt10(5)).toBe(5 < 10);
    });
});
