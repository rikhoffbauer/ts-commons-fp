const compose = (...funcs) => value =>
    funcs.reduce((value, func) => func(value), value);

module.exports = compose(require("jest-junit"), require("jest-stare"));
