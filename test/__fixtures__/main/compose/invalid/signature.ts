import compose from "../../../../../src/util/compose";

interface State {
    deeper: {
        deepest: string;
    };
}

const rootState: State = { deeper: { deepest: "" } };

const getDeeper = (state: State) => state.deeper;
const getDeepest = (state: State["deeper"]) => state.deepest;

const getDeepestFromRoot = compose(getDeepest, getDeeper);

getDeepestFromRoot("");
