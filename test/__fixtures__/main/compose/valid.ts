import { compose } from "../../../../src/util";

interface State {
    deeper: {
        deepest: string;
    };
}

const rootState: State = { deeper: { deepest: "" } };

const getDeeper = (state: State): State["deeper"] => state.deeper;
const getDeepest = (state: State["deeper"]): State["deeper"]["deepest"] =>
    state.deepest;

const getDeepestFromRoot = compose(getDeepest, getDeeper);

const value: string = getDeepestFromRoot(rootState);
//const value: number = getDeepestFromRoot(rootState);
