import { pipe } from "../../../../src/util";

interface State {
    deeper: {
        deepest: string;
    };
}

const rootState: State = { deeper: { deepest: "" } };

const getDeeper = (state: State) => state.deeper;
const getDeepest = (state: State["deeper"]) => state.deepest;

const getDeepestFromRoot = pipe(getDeeper, getDeepest);

const value: string = getDeepestFromRoot(rootState);
//const value: number = getDeepestFromRoot(rootState);
