import pipe from "../../../../../src/util/pipe";

interface State {
    deeper: {
        deepest: number;
    };
}

const rootState: State = { deeper: { deepest: 0 } };

const getDeeper = (state: State) => state.deeper;
const getDeepest = (state: State["deeper"]) => state.deepest;

const getDeepestFromRoot = pipe(getDeeper, getDeepest);

getDeepestFromRoot("");
