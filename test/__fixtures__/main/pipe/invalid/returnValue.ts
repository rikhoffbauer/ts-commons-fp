import pipe from "../../../../../src/util/pipe";

interface State {
    deeper: {
        deepest: string;
    };
}

const rootState: State = { deeper: { deepest: "" } };

const getDeeper = (state: State) => state.deeper;
const getDeepest = (state: State["deeper"]) => state.deepest;

const getDeepestFromRoot = pipe(getDeeper, getDeepest);

const value: number = getDeepestFromRoot(rootState);
