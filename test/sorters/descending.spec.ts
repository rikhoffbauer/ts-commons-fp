import descending from "../../src/sorters/descending";

describe("descending(a, b)", () => {
    it("should sort descending", () => {
        expect([1, 2, 3].sort(descending)).toEqual([3, 2, 1]);
    });
    it("should put undefined values at the end of the array", () => {
        expect(([1, undefined, 2, 3] as number[]).sort(descending)).toEqual([
            3,
            2,
            1,
            undefined,
        ]);
    });
});
