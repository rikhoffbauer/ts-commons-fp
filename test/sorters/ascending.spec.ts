import ascending from "../../src/sorters/ascending";

describe("ascending(a, b)", () => {
    it("should sort ascending", () => {
        expect([3, 2, 1].sort(ascending)).toEqual([1, 2, 3]);
    });
    it("should put undefined values at the end of the array", () => {
        expect(([3, undefined, 2, 1] as number[]).sort(ascending)).toEqual([
            1,
            2,
            3,
            undefined,
        ]);
    });
});
