import call from "../../src/function/call";

describe("call(...args)(fn)", () => {
    it("should call all the functions with the arguments and return an array with the results", () => {
        const add = (val1: number, val2: number) => val1 + val2;
        const results = call(2, 1)(add);

        expect(results).toEqual(3);
    });
});
