import callAll from "../../src/function/callAll";

describe("callAll(...fns)(...args)", () => {
    it("should call all the functions with the arguments and return an array with the results", () => {
        const add = (val1: number, val2: number) => val1 + val2;
        const divide = (val1: number, val2: number) => val1 / val2;
        const results = callAll(add, divide)(2, 1);

        expect(results).toEqual([3, 2]);
    });
});
