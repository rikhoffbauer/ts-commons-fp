import invertFunction from "../../src/function/invertFunction";

describe("invertFunction(fn, depth)", () => {
    it("should return a function that is inverted to the provided depth", () => {
        const divide = (divisor: number) => (dividend: number) =>
            dividend / divisor;
        const invertedDivide = invertFunction(divide, 2);

        expect(divide(2)(1)).toEqual(invertedDivide(1)(2));
    });

    it("should default to a depth of 2", () => {
        const divide = (divisor: number) => (dividend: number) =>
            dividend / divisor;
        const invertedDivide = invertFunction(divide);

        expect(divide(2)(1)).toEqual(invertedDivide(1)(2));
    });
});
