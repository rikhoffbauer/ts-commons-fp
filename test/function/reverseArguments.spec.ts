import reverseArguments from "../../src/function/reverseArguments";

describe("reverseArguments(fn)", () => {
    it("should reverse the argument order of a function", () => {
        const divide = (divisor: number, dividend: number) =>
            dividend / divisor;
        const invertedDivide = reverseArguments(divide);

        expect(divide(2, 1)).toEqual(invertedDivide(1, 2));
    });
});
