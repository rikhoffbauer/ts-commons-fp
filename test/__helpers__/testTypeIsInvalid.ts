import testTypeIsValid from "./testTypeIsValid";

const testTypeIsInvalid = async (path: string) => {
    try {
        await testTypeIsValid(path);
        throw new Error(`Types are not invalid`);
    } catch (err) {
        return;
    }
};

export default testTypeIsInvalid;
