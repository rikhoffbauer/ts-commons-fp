import { exec } from "child_process";

const testTypeIsValid = (path: string) =>
    new Promise((resolve, reject) =>
        exec(
            `tsc --noEmit --lib es2017 ${path}`,
            err => (err ? reject(err) : resolve()),
        ),
    );

export default testTypeIsValid;
