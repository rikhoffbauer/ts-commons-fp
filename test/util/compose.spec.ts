import { compose, identity } from "../../src/util/index";
import testTypeIsInvalid from "../__helpers__/testTypeIsInvalid";
import testTypeIsValid from "../__helpers__/testTypeIsValid";

const add = (right: number) => (left: number) => left + right;
const divide = (divisor: number) => (dividend: number) => dividend / divisor;

describe(`compose(...fns)`, () => {
    it(`should compose the provided functions`, async () => {
        const add3AndDivideBy2 = compose(divide(2), add(3));
        const val = 3;

        expect(add3AndDivideBy2(val)).toBe(divide(2)(add(3)(val)));
    });

    it(`should pass all (more than 1) arguments passed initially to the first executed (last argument) function`, async () => {
        const firstFn = jest.fn((...args: any[]) => args[0]);
        const secondFn: typeof identity = jest.fn(identity);
        const fn = compose(secondFn, firstFn);
        const args = [1, 2, 3];

        fn(...args);

        expect(firstFn).toHaveBeenCalledWith(...args);
        expect(secondFn).toHaveBeenCalledWith(args[0]);
    });

    it(`should ignore non function arguments`, async () => {
        // the any casts are necessary
        // because this kind of usage is prevented by the types as well
        const add3AndDivideBy2: any = compose(
            divide(2),
            null as any,
            "asdasd" as any,
            add(3),
            true as any,
            123 as any,
            undefined as any,
        );
        const val = 3;

        expect(add3AndDivideBy2(val)).toBe(divide(2)(add(3)(val)));
    });

    describe("types", () => {
        it("should not throw compiler errors when used correctly", async () => {
            await expect(
                testTypeIsValid(`./test/__fixtures__/main/compose/valid.ts`),
            ).resolves.toBeUndefined();
        });

        it("should have the correct return type", async () => {
            await expect(
                testTypeIsInvalid(
                    `./test/__fixtures__/main/compose/invalid/returnValue.ts`,
                ),
            ).resolves.toBeUndefined();
        });

        it("should have the correct signature", async () => {
            await expect(
                testTypeIsInvalid(
                    `./test/__fixtures__/main/compose/invalid/signature.ts`,
                ),
            ).resolves.toBeUndefined();
        });
    });
});
