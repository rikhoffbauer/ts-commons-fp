import curry, { __ } from "../../src/util/curry";

describe(`curry`, () => {
    it(`should return the provided function if it doesnt take at least 2 arguments`, () => {
        const fn2 = (a: number): number => a;
        const curried2 = curry(fn2);

        expect(curried2).toBe(fn2);
        expect(curried2(1)).toBe(fn2(1));
    });

    it(`should work return a function that takes the missing arguments`, () => {
        const fn = (a: string, b: number): boolean => a.length === b;
        const curried = curry(fn);

        expect(curried("A")).toBeInstanceOf(Function);
        expect(curried("A")(1)).toBe(fn("A", 1));
        expect(curried("A", 1)).toBe(fn("A", 1));
    });

    it(`should allow for skipping arguments by providing the placeholder`, () => {
        const fn = (a: string, b: number): boolean => a.length === b;
        const curried = curry(fn);

        expect(curried(__, 1)).toBeInstanceOf(Function);
        expect(curried(__, 1)("A")).toBe(fn("A", 1));
    });

    it("should be able to be called multiple times", () => {
        const fn = (a: string, b: number): boolean => a.length === b;
        const curried = curry(fn);
        const has3letters = curried(__, 3);

        expect(has3letters("ASD")).toBe(true);
        expect(has3letters("ASD")).toBe(true);
        expect(has3letters("a")).toBe(false);
    });

    it("should work with 3 arguments", () => {
        const fn = (a: string, b: number, c: number): boolean =>
            a.length - 3 === b;
        const curried = curry(fn);
        const asd = curried("ASD");
        const asd2 = curried("ASD", 0);
        asd2(3) == false;
        asd(0, 3) === true;
        asd(1)(3) === false;
        const has3letters = curried(__, 0, 3);
        const has3letters2 = curried(__, 0);

        expect(has3letters2("ASD", 3)).toBe(true);
        expect(has3letters2("ASD")(3)).toBe(true);
        expect(has3letters("ASD")).toBe(true);
        expect(has3letters("a")).toBe(false);
    });

    it("should work with 4 arguments", () => {
        const fn = (a: string, b: number, c: number, d: boolean): boolean =>
            d || a.length - 3 === b;
        const curried = curry(fn);
        const asd = curried("ASD");
        const asd2 = curried("ASD", 0);
        asd2(3, false) == false;
        asd(0, 3, false) === true;
        asd(1)(3)(true) === false;
        const has3letters = curried(__, 0, 3);
        const has3letters2 = curried(__, 0);

        expect(has3letters2("ASD", 3, false)).toBe(true);
        expect(has3letters2("ASD")(3)(false)).toBe(true);
        expect(has3letters("ASD", false)).toBe(true);
        expect(has3letters("ASD", false)).toBe(true);
        expect(has3letters("a", false)).toBe(false);
    });

    it("should work with 10 arguments", () => {
        const fn = (
            a: 1,
            b: 2,
            c: 3,
            d: 4,
            e: 5,
            f: 6,
            g: 7,
            h: 8,
            i: 9,
            j: 10,
        ): number => a / b * c / d * e / f * g / h * i / j;
        const result = fn(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        const curried = curry(fn);

        expect(curried(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)).toBe(result);
        expect(curried(__, 2, __, 4)(1, 3)(5, __, 7)(6, 8, 9)(10)).toBe(result);
        expect(curried(__, __, __, 4)(1, 2, 3)(5, __, 7)(6, 8)(9)(10)).toBe(
            result,
        );
        expect(curried(__, 2, __, 4, 5)(1, 3)(__, 7)(6, 8, 9)(10)).toBe(result);
        expect(curried(__, __, __, 4, 5)(1, 2, 3)(__, 7)(6, 8)(9)(10)).toBe(
            result,
        );
    });
});
