import pipe from "../../src/util/pipe";
import testTypeIsInvalid from "../__helpers__/testTypeIsInvalid";
import testTypeIsValid from "../__helpers__/testTypeIsValid";

const add = (right: number) => (left: number) => left + right;
const divide = (divisor: number) => (dividend: number) => dividend / divisor;

describe(`pipe(...fns)`, () => {
    it(`should pipe the provided functions`, async () => {
        const add3AndDivideBy2 = pipe(add(3), divide(2));

        const val = 3;

        expect(add3AndDivideBy2(val)).toBe(divide(2)(add(3)(val)));
    });

    it(`should pass all (more than 1) arguments passed initially to the first executed (last argument) function`, async () => {
        const firstFn = jest.fn((...args: any[]) => args[0]);
        const secondFn: Function = jest.fn(v => v);
        const fn = pipe(firstFn, secondFn);
        const args = [1, 2, 3];

        fn(...args);

        expect(firstFn).toHaveBeenCalledWith(...args);
        expect(secondFn).toHaveBeenCalledWith(args[0]);
    });

    it(`should ignore non function arguments`, async () => {
        // the any casts are necessary
        // because this kind of usage is prevented by the types as well
        const add3AndDivideBy2: any = pipe(
            add(3),
            null as any,
            divide(2),
            "asdasd" as any,
            true as any,
            123 as any,
            undefined as any,
        );
        const val = 3;

        expect(add3AndDivideBy2(val)).toBe(divide(2)(add(3)(val)));
    });

    describe("types", () => {
        it("should not throw compiler errors when used correctly", async () => {
            await expect(
                testTypeIsValid(`./test/__fixtures__/main/pipe/valid.ts`),
            ).resolves.toBeUndefined();
        });

        it("should have the correct return type", async () => {
            await expect(
                testTypeIsInvalid(
                    `./test/__fixtures__/main/pipe/invalid/returnValue.ts`,
                ),
            ).resolves.toBeUndefined();
        });

        it("should have the correct signature", async () => {
            await expect(
                testTypeIsInvalid(
                    `./test/__fixtures__/main/pipe/invalid/signature.ts`,
                ),
            ).resolves.toBeUndefined();
        });
    });
});
