import { compose, identity } from "../../src/util/index";

describe(`identity`, () => {
    it(`should return the value that is passed in`, async () => {
        const val = {};

        expect(identity(val)).toBe(val);
    });
});
