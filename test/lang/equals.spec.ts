import equals from "../../src/lang/equals";

describe("equals(value)(value2)", () => {
    it("should return true if the values are equal (triple eq; ===)", () => {
        expect(equals(1)(1)).toBe(true);
        expect(equals({})({})).toBe(false);
        expect(equals(1)("1" as any)).toBe(false);
    });
});
