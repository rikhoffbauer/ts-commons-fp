import toString from "../../src/lang/toString";

describe("toString(value)", () => {
    it("should convert the provided value to a string by calling its toString method", () => {
        const string = "string";
        const value = { toString: jest.fn(() => string) };

        expect(toString(value)).toBe(string);
    });

    it("should return an empty string for null and undefined values", () => {
        expect(toString(null)).toBe("");
        expect(toString(undefined)).toBe("");
    });
});
