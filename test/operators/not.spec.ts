import not from "../../src/operators//not";

describe("not(predicate)(value)", () => {
    it("should invert the return value of the predicate", () => {
        const isPositive = (v: number) => v > 0;
        const isNotPositive = not(isPositive);

        expect(isNotPositive(1)).toBe(false);
        expect(isNotPositive(0)).toBe(true);
        expect(isNotPositive(-1)).toBe(true);
    });
});
