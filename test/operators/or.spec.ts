import or from "../../src/operators//or";

describe("or(...predicates)(value)", () => {
    it("should only return true if at least one of the predicates pass", () => {
        const isPositive = (v: number) => v > 0;
        const isZero = (v: number) => v === 0;
        const isZeroOrPositive = or(isPositive, isZero);

        expect(isZeroOrPositive(1)).toBe(true);
        expect(isZeroOrPositive(0)).toBe(true);
        expect(isZeroOrPositive(-1)).toBe(false);
    });
});
