import and from "../../src/operators/and";

describe("and(...predicates)(value)", () => {
    it("should only return true if all the predicates pass", () => {
        const smallerThan = (n: number) => (v: number) => v < n;
        const greaterThan = (n: number) => (v: number) => v > n;
        const between = (min: number, max: number) =>
            and(greaterThan(min), smallerThan(max));

        const isTwo = between(1, 3);
        expect(isTwo(2)).toBe(true);
        expect(isTwo(1)).toBe(false);
        expect(isTwo(3)).toBe(false);
    });
});
