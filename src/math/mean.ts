import reduce from "../array/reduce";
import total from "./total";

const mean = (arr: number[]) =>
    arr.length ? reduce(total)(arr) / arr.length : NaN;

export default mean;
