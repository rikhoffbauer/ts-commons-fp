const gte = (rightOperand: number) => (leftOperand: number) =>
    leftOperand >= rightOperand;

export default gte;
