import countOccurrences from "../array/countOccurrences";
import getByIndex from "../array/getByIndex";
import map from "../array/map";
import unique from "../array/unique";
import call from "../function/call";
import pipe from "../util/pipe";

/**
 * Gets the mode of an array (the values that occur most)
 * @param {T[]} arr the array to get the mode for
 * @returns {T[]} the values that occur most
 */
const mode = <T>(arr: T[]): T[] => {
    const occurrences = map(pipe(countOccurrences, call(arr)))(arr);
    let maxIndexes: number[] = [];
    let maxOccurrence = 1;

    occurrences.forEach((occurrence, index) => {
        if (occurrence <= 1 || occurrence < maxOccurrence) {
            return;
        }

        if (occurrence === maxOccurrence) {
            maxIndexes = [...maxIndexes, index];
            return;
        }

        maxIndexes = [index];
        maxOccurrence = occurrence;
    });

    // no mode
    if (!maxIndexes.length) {
        return [];
    }

    return pipe(map(pipe(getByIndex, call(arr))), unique)(maxIndexes);
};

export default mode;
