//noinspection ES6UnusedImports
import { Curried2 } from "../../generated/curry";
import curry from "../util/curry";

const subtract = (amount: number, number: number) => number - amount;

export default curry(subtract);
