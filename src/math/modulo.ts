//noinspection ES6UnusedImports
import { Curried2 } from "../../generated/curry";
import curry from "../util/curry";

const modulo = (divisor: number, dividend: number): number =>
    dividend % divisor;

export default curry(modulo);
