const lt = (rightOperand: number) => (leftOperand: number) =>
    leftOperand < rightOperand;

export default lt;
