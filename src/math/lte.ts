//noinspection ES6UnusedImports
import { Curried2 } from "../../generated/curry";
import curry from "../util/curry";

const lte = (rightOperand: number, leftOperand: number) =>
    leftOperand <= rightOperand;

export default curry(lte);
