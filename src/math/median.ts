import getCenterItem from "../array/getCenterItem";
import sort from "../array/sort";
import ascending from "../sorters/ascending";
import pipe from "../util/pipe";
import isEven from "./isEven";
import mean from "./mean";

const median = (arr: number[]) =>
    isEven(arr.length) ? mean(arr) : pipe(sort(ascending), getCenterItem)(arr);

export default median;
