import { Reducer } from "../types";

const total: Reducer<number, number> = (result = 0, value) => result + value;

export default total;
