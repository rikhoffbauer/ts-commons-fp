const range = (arr: number[]) => Math.max(...arr) - Math.min(...arr);

export default range;
