import not from "../operators/not";
import isEven from "./isEven";

const isOdd = not(isEven);

export default isOdd;
