import pipe from "../util/pipe";
import isZero from "./isZero";
import modulo from "./modulo";

const isEven = pipe(modulo(2), isZero);

export default isEven;
