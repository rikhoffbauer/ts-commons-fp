import equals from "../lang/equals";

const isZero = equals(0);

export default isZero;
