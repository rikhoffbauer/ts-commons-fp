const max = (arr: number[]) => Math.max(...arr);

export default max;
