//noinspection ES6UnusedImports
import { Curried2 } from "../../generated/curry";
import curry from "../util/curry";

/**
 * Multiply a number with another number.
 * ### Simple example
 *  ```
 *  import { multiply } from "@ts-commons/fp/math";
 *  import { __ } from "@ts-commons/fp";
 *  multiply(2, 3); // 6
 *  multiply(2)(3); // 6
 *  multiply(__, 3)(2); // 6
 *  ```
 */
const multiply = curry((multiplier: number, multiplicand: number): number =>
    multiplicand * multiplier);

export default multiply;
