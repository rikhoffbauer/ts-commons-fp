const divide = (divisor: number) => (dividend: number): number =>
    dividend / divisor;

export default divide;
