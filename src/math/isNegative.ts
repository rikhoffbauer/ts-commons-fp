import lt from "./lt";

const isNegative = lt(0);

export default isNegative;
