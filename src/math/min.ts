const min = (arr: number[]) => Math.min(...arr);

export default min;
