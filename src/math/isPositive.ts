//noinspection ES6UnusedImports
import { Curried1, Curried2 } from "../../generated/curry";
import gt from "./gt";

const isPositive = gt(0);

export default isPositive;
