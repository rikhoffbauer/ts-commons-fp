import { Predicate } from "../types";

const and = <T extends {}>(...predicates: Predicate<T>[]): Predicate<T> => (
    v,
    index,
    arr,
) => predicates.every((predicate: Predicate<T>) => predicate(v, index, arr));

export default and;
