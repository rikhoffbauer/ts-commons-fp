import { Predicate } from "../types";

const not = <T extends {}>(predicate: Predicate<T>): Predicate<T> => v =>
    !predicate(v);

export default not;
