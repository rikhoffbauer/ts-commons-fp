import { Predicate } from "../types";

const or = <T extends {}>(...predicates: Predicate<T>[]): Predicate<T> => v =>
    predicates.some(predicate => predicate(v));

export default or;
