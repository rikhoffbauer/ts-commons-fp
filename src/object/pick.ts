import pipe from "../util/pipe";
import reduce from "../array/reduce";
import includedIn from "../array/includedIn";
import pickReducer from "./_pickReducer";
import filter from "../array/filter";
import map from "../array/map";
import invertFunction from "../function/invertFunction";
import toString from "../lang/toString";
import getKeys from "./getKeys";

const pick = Object.assign(
    <T, T2 extends keyof T = keyof T>(...keys: T2[]) => (obj: T): Pick<T, T2> =>
        pipe(
            getKeys,
            filter(includedIn(map(toString)(keys))),
            reduce(pickReducer(obj), {} as T),
        )(obj),
    {
        inverted: <T, T2 extends keyof T>(obj: T) =>
            invertFunction<
                typeof pick,
                (obj: T) => (...keys: T2[]) => Pick<T, T2>
            >(pick)(obj),
    },
);

export default pick;
