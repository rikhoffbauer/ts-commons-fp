import map from "../array/map";
import pipe from "../util/pipe";
import getKeys from "./getKeys";

const getValues = <T>(obj: { [name: string]: T }): T[] =>
    pipe(getKeys, map(key => obj[key]))(obj);

export default getValues;
