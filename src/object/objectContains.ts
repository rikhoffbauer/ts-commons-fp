import every from "../array/every";
import equals from "../lang/equals";
import pipe from "../util/pipe";
import getKeys from "./getKeys";
import getProperty from "./getProperty";

const objectContains = <T>(properties: Partial<T>) => (obj: T): boolean =>
    pipe(
        getKeys,
        every<keyof T>(key => pipe(
            getProperty<T>(key),
            equals(getProperty<T>(key)(properties as T)),
        )(obj)),
    )(properties);

export default objectContains;
