import pipe from "../util/pipe";
import includedIn from "../array/includedIn";
import not from "../operators/not";
import filter from "../array/filter";
import map from "../array/map";
import toString from "../lang/toString";
import getKeys from "./getKeys";
import pick from "./pick";

export type Omit<T, K extends keyof T> = Pick<
    T,
    ({ [P in keyof T]: P } &
        { [P in K]: never } & {
            [x: string]: never;
            [x: number]: never;
        })[keyof T]
>;

const omit = <T, T2 extends keyof T>(...keys: T2[]) => (obj: T): Omit<T, T2> =>
    pipe(
        getKeys,
        filter(not(includedIn(map(toString)(keys)))),
        pick.inverted(obj),
    )(obj);

export default omit;
