import { Reducer } from "../types/reducer";

/**
 * The pick reducer can be used to reduce an array of property names to a subset of an object.
 *
 * ### Simple example
 *  ```
 *  import { reduce } from "@ts-commons/fp";
 *  import { pickReducer } from "@ts-commons/fp/reducers";
 *  const obj = { a: 1, b: 2 };
 *  ["a"].reduce(pickReducer(obj)); // { a: 1 }
 *  // or
 *  reduce(pickReducer(obj)(["a"]); // { a: 1 }
 *  ```
 */
const pickReducer = <T extends { [name: string]: any }, T2 extends keyof T>(
    obj: T,
): Reducer<T, T2> => (result, key): T =>
    ({ ...(result as {}), [key]: obj[key] } as T);

export default pickReducer;
