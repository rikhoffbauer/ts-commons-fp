const getProperty = <T, T2 extends keyof T = keyof T>(property: T2) => (obj: T): T[T2] =>
    obj[property];

export default getProperty;
