const getKeys = Object.keys as <T, T2 extends keyof T = keyof T>(obj: T) => T2[];

export default getKeys;
