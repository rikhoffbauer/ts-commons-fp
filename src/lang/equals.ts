const equals = <T>(v1: T) => (v2: T) => v1 === v2;

export default equals;
