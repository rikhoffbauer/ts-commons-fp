import { FixedOutputTransformer } from "../types/transformer";

const toString: FixedOutputTransformer<string> = val =>
    val == null ? "" : val.toString();

export default toString;
