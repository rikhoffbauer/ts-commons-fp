import { Transformer } from "../types/transformer";

const pluck = <T, T2 extends keyof T>(
    propertyName: T2,
): Transformer<T[], T[T2][]> => arr => arr.map(v => v[propertyName]);

export default pluck;
