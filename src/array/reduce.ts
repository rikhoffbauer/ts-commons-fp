import { Transformer } from "../types";
import { Reducer } from "../types/reducer";

const reduce = <Result, Value>(
    reducer: Reducer<Result, Value>,
    initialValue?: Result,
): Transformer<Value[], Result> => arr =>
    arr.reduce(reducer as any, initialValue) as any;

export default reduce;
