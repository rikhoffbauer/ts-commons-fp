const pop = <T>(arr: T[]) => arr.pop();

export default pop;
