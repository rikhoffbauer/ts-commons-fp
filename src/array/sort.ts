import { Sorter } from "../types/sorter";
import { Transformer } from "../types/transformer";

const sort = <T>(sortFn: Sorter<T>): Transformer<T[]> => arr =>
    arr.sort(sortFn);

export default sort;
