import equals from "../lang/equals";
import count from "./count";

const countOccurrences = <T>(value: T) => (arr: T[]): number =>
    count(equals(value))(arr);

export default countOccurrences;
