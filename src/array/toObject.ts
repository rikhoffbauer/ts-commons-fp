import reduce from "./reduce";

const toObject = <T, T2 extends keyof T>(idProperty: T2) => (
    arr: T[],
): { [name: string]: T } => {
    return reduce<{ [name: string]: T }, T>((result, value) => ({
        ...result,
        [value[idProperty] as any]: value,
    }))(arr);
};

export default toObject;
