const includes = <T extends {}>(value: T) => (arr: T[]) => arr.includes(value);

export default includes;
