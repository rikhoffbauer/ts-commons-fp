import { Predicate } from "../types";
import { Transformer } from "../types/transformer";

const every = <T>(predicate: Predicate<T>): Transformer<T[], boolean> => arr =>
    arr.every(predicate);

export default every;
