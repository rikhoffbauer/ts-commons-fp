import { Transformer } from "../types";

const getByIndex = <T>(index: number): Transformer<T[], T> => arr => arr[index];

export default getByIndex;
