import { Predicate } from "../types";
import filter from "./filter";

/**
 * Count matches of a predicate against an array.
 * ### Simple example
 *  ```
 *  import { count } from "@ts-commons/fp/array";
 *  count(v => v % 2 === 0)([1,2,3]); // 1
 *  ```
 * @param {Predicate<T>} predicate
 * @returns {Function}
 */
const count = <T>(predicate: Predicate<T>) => (arr: T[]) =>
    filter(predicate)(arr).length;

export default count;
