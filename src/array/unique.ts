import includes from "./includes";
import reduce from "./reduce";

let uniqueReducer = <T>(result: T[] = [], value: T) => {
    if (includes(value)(result)) {
        return result;
    }

    return [...result, value];
};

const unique = <T>(arr: T[]) => reduce<T[], T>(uniqueReducer)(arr);

export default unique;
