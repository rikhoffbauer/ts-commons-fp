import { MapFn } from "../types/map";
import { Transformer } from "../types/transformer";
import pipe from "../util/pipe";

const map = <T, T2 = T>(
    ...mapFns: MapFn<T, T2>[]
): Transformer<T[], T2[]> => arr => arr.map(pipe(...mapFns));

export default map;
