import isEven from "../math/isEven";

const getCenterIndex = <T>(arr: T[]) =>
    isEven(arr.length) ? undefined : (arr.length - 1) / 2;

export default getCenterIndex;
