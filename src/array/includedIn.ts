const includedIn = <T extends {}>(arr: T[]) => (value: T) =>
    arr.includes(value);

export default includedIn;
