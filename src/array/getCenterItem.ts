import pipe from "../util/pipe";
import getByIndex from "./getByIndex";
import getCenterIndex from "./getCenterIndex";

const getCenterItem = <T>(arr: T[]): T =>
    pipe(getCenterIndex, getByIndex)(arr)(arr) as any;

export default getCenterItem;
