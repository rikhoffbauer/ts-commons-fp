const slice = <T>(start: number, end?: number) => (arr: T[]) =>
    arr.slice(start, end);

export default slice;
