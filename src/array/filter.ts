import { Predicate } from "../types";
import { Transformer } from "../types/transformer";
import pipe from "../util/pipe";

const filter = <T>(...predicates: (Function|Predicate<T>)[]): Transformer<T[]> => arr =>
    arr.filter(pipe(...predicates));

export default filter;
