export type GenericReducer = <Result, Value>(
    result: Result,
    value: Value,
) => Result;
export type Reducer<Result, Value> = (result: Result, value: Value) => Result;
