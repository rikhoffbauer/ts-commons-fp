export type Transformer<Input, Output = Input> = (value: Input) => Output;
export type FixedOutputTransformer<Output> = <Input>(value: Input) => Output;
export type FixedInputTransformer<Input> = <Output>(value: Input) => Output;
