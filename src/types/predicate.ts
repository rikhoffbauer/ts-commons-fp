export type Predicate<T> = ((value: T, index?: number, arr?: T[]) => boolean);
