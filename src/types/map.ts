export type MapFn<Input, Output = Input> = (
    value: Input,
    index?: number,
    arr?: Input[],
) => Output;
