import { Curry, Placeholder } from "../../generated/curry";

/**
 * __ can be used in calls to curried functions to indicate that the argument still needs to be provided.
 * This is useful when you want to call a curried function only with second argument for example.
 * ### Simple example
 *  ```typescript
 *  import { curry, __ } from "@ts-commons/fp";
 *  const fn = curry((a: number, b: number, c: number) => {
 *      return (a + b) / c;
 *  });
 *  fn(1, 2, 3);
 *  // can be written as
 *  fn(1, __, 3)(2);
 *  fn(__, 2, 3)(1);
 *  fn(__, __, 3)(1, 2);
 *  fn(__, __, 3)(1)(2);
 *  ```
 * @type {Placeholder}
 * @private
 */
export const __: Placeholder = { __curry_placeholder: "PLACEHOLDER" };

const curry: Curry = ((fn: Function) => {
    const argumentCount = fn.length;

    if (argumentCount < 2) {
        return fn;
    }

    const availableIndexes = [];

    while (availableIndexes.length < argumentCount) {
        availableIndexes.push(availableIndexes.length);
    }

    const curried = (
        availableIndexes: number[],
        values: { [index: number]: any },
        ...args: any[]
    ) => {
        const _availableIndexes = [...availableIndexes];
        const _values = { ...values };
        let offset = 0;

        args.forEach(arg => {
            if (arg === __) {
                offset += 1;
                return;
            }

            const [index] = _availableIndexes.splice(offset, 1);

            _values[index] = arg;
        });

        if (!_availableIndexes.length) {
            const args = Object.keys(_values)
                .sort((a, b) => Number(a) - Number(b))
                .map(key => _values[Number(key)]);

            return fn(...args);
        }

        return curried.bind(null, _availableIndexes, _values);
    };

    return curried(availableIndexes, []);
}) as any;

export default curry;
