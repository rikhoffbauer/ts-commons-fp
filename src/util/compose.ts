import { Compose } from "../../generated/compose";
import identity from "./identity";

/**
 * `compose` composes the provided functions.
 * ### Simple example
 *  ```
 *  import { compose } from "@ts-commons/fp";
 *  const add = (right: number) => (left: number) => left + right;
 *  const divide = (divisor: number) => (dividend: number) => dividend / divisor;
 *  const add3AndDivideBy2 = compose(divide(2), add(3));
 *  add3AndDivideBy2(3); // returns 3, because divide(2)(add(3)(3)) is 3
 *  ```
 *  @returns {Function}
 */
const compose: Compose = (...fns: Function[]) => (...args: any[]) => {
    const _fns = [...fns];

    return _fns.reduceRight(
        (val, fn) => (typeof fn === "function" ? fn(val) : val),
        (_fns.pop() || identity)(...args),
    );
};

export default compose;
