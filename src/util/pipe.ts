import { Pipe } from "../../generated/pipe";
import compose from "./compose";

const pipe: Pipe = (...fns: Function[]) => compose(...fns.reverse());

export default pipe;
