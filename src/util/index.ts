export { default as compose } from "./compose";
export { default as pipe } from "./pipe";
export { default as identity } from "./identity";
export { default as curry, __ } from "./curry";
