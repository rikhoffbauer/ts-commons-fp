import { Sorter } from "../types/sorter";

const ascending: Sorter<number> = (a, b) => a - b;

export default ascending;
