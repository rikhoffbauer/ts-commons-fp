import { Sorter } from "../types/sorter";

const descending: Sorter<number> = (a, b) => b - a;

export default descending;
