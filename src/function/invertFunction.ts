/**
 * Turns a function inside out.
 *
 * E.g.:
 * a => b => a + b;
 * Becomes:
 * b => a => a + b;
 * Using:
 * invertFunction(2)(a => b => a + b);
 *
 * The number 2 passed to invertFunction indicates the depth of the function passed in.
 *
 * arg1 => arg2 => val
 * has depth 2.
 *
 * arg1 => arg2 => arg3 => val
 * has depth 3.
 *
 * etc.
 */
const invertFunction = <T extends Function, T2 extends Function>(
    func: T,
    depth = 2,
): T2 => {
    const insideOut = (argArrays = [] as any[][], layerCount = 0) => (
        ...args: any[]
    ) =>
        layerCount === depth - 1
            ? argArrays.reduceRight(
                  (_func, args) => _func(...args),
                  func(...args),
              )
            : insideOut([...argArrays, args], layerCount + 1);

    return insideOut() as any;
};

export default invertFunction;
