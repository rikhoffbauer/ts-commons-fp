export { default as call } from "./call";
export { default as callAll } from "./callAll";
export { default as invertFunction } from "./invertFunction";
export { default as reverseArguments } from "./reverseArguments";
