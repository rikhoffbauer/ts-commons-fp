/**
 * Reverses the arguments of a function.
 *
 * E.g.:
 * (a, b) => a / b;
 * Becomes:
 * (b, a) => a / b;
 * Using:
 * reverseArguments((a, b) => a + b);
 *
 * @param {T} func
 * @returns {T2}
 */
const reverseArguments = <
    T extends Function = Function,
    T2 extends Function = Function
>(
    func: T,
): T2 => ((...args: any[]) => func(...args.reverse())) as any;

export default reverseArguments;
