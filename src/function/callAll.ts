const callAll = <
    T extends (...args: any[]) => T2 = (...args: any[]) => T2,
    T2 = any
>(
    ...funcs: T[]
) => (...args: any[]) => funcs.map(fn => fn(...args) as T2);

export default callAll;
