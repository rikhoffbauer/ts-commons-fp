const call = <
    T extends (...args: any[]) => T2 = (...args: any[]) => T2,
    T2 = any
>(
    ...args: any[]
) => (fn: T) => fn(...args);

export default call;
