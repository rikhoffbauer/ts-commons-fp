# @ts-commons/fp

![coverage](https://gitlab.com/rikhoffbauer/ts-commons-fp/badges/master/coverage.svg)
![build](https://gitlab.com/rikhoffbauer/ts-commons-fp/badges/master/build.svg)
![npm version](https://badge.fury.io/js/%40ts-commons%2Ffp.svg)

A collection of function programming utilities written in TypeScript.

## Installing

##### Using npm

`npm install @ts-commons/fp`

##### Using yarn

`yarn add @ts-commons/fp`

## Usage

```typescript
import { compose } from "@ts-commons/fp";
import { add, divide } from "@ts-commons/fp/math";

const add3AndDivideBy2 = compose(divide(2), add(3));

add3AndDivideBy2(3); // returns 3 ((3 + 3) / 2)
```

For more information see the [API documentation](https://rikhoffbauer.gitlab.io/ts-commons-fp).

## Contributing

Contributions in the form of pull requests and bug reports are appreciated.

### Running tests

Tests are ran using the `test` npm script.

##### Using npm

`npm test`

##### Using yarn

`yarn test`
