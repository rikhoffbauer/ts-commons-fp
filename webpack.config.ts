import { execSync } from "child_process";
import { join } from "path";
import { Configuration } from "webpack";

const CleanWebpackPlugin = require("clean-webpack-plugin");
const CreateFileWebpack = require("create-file-webpack");
const WebpackShellPlugin = require("webpack-shell-plugin");
const getPath = (...segments: string[]) => join(__dirname, ...segments);

const entries = [
    "array",
    "function",
    "lang",
    "math",
    "object",
    "operators",
    "sorters",
    "types",
    "util",
];

const paths = {
    outDir: getPath("lib"),
    outFile: "[name].js",
};

// TODO find a better way to incorporate this into the webpack build
// webpack-shell-plugin doesn't seem to wait for the command to finish before
// starting the webpack build...
execSync(`npm run generate-types`);

const config: Configuration = {
    entry: entries.reduce(
        (paths, entry) => ({
            ...(paths as {}),
            [entry]: getPath(`src/${entry}/index.ts`),
        }),
        {},
    ),
    devtool: "source-map",
    mode: "production",
    target: "node",
    output: {
        path: paths.outDir,
        libraryTarget: "commonjs2",
        filename: paths.outFile,
    },
    resolve: {
        extensions: [".ts", ".js", ".json"],
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: "awesome-typescript-loader",
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin([
            "lib",
            "js",
            ...entries.map(entry => `./${entry}.d.ts`),
            ...entries.map(entry => `./${entry}.js`),
        ]),
        ...entries.map(
            entry =>
                new CreateFileWebpack({
                    path: "./",
                    fileName: `${entry}.js`,
                    content: `module.exports = require("./lib/${entry}");`,
                }),
        ),
        ...entries.map(
            entry =>
                new CreateFileWebpack({
                    path: "./",
                    fileName: `${entry}.d.ts`,
                    content: `export * from "./js/src/${entry}/index";`,
                }),
        ),
        new WebpackShellPlugin({
            safe: true,
            onBuildEnd: ["rm -rf js && node_modules/.bin/tsc"],
        }),
    ],
};

export default config;
