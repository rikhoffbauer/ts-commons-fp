import identity from "../src/util/identity";

export const range = (start: number, end: number) => {
    const arr: number[] = [];

    for (let i = start; i <= end; i++) {
        arr.push(i);
    }

    return arr;
};
export const compose = (...fns: Function[]) => (...args: any[]) => {
    const _fns = [...fns];

    return _fns.reduceRight(
        (val, fn) => (typeof fn === "function" ? fn(val) : val),
        (_fns.pop() || identity)(...args),
    );
};
export const pipe = (...fns: Function[]) => compose(...fns.reverse());
export const append = (suffix: string) => (str: string) => str + suffix;
export const prepend = (prefix: string) => (str: string) => prefix + str;
export const prependWithSpace = prepend(" ");
export const indent = (size: number) => (str: string) =>
    range(1, size).reduce(prependWithSpace, str);
