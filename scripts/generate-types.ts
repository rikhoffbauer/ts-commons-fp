import { writeFile } from "fs";
import mkdirp from "mkdirp";
import generateCurryTypes from "./generate-curry-types";
import { dirname, join } from "path";

const createArgumentsToArrayOverload = (argumentCount: number = 20) => {
    const generics = [];
    const args = [];
    const indent = `    `;

    for (let argIndex = 0; argIndex < argumentCount; argIndex++) {
        const argType = `A${argIndex + 1}`;
        const argName = `arg${argIndex + 1}`;
        generics.push(argType);
        args.push(`${argName}: ${argType}`);
    }

    return `${indent}<${generics.join(", ")}>(): (${args.join(
        ", ",
    )}) => [${generics.join(", ")}]`;
};

const createComposeOverload = (
    composeArgCount: number = 20,
    signatureArgumentCount: number = 5,
) => {
    const indent = `    `;
    const generics = [];
    const fns = [];
    const fnBaseName = `fn`;
    const argumentBaseName = `arg`;
    const returnValueGenericBaseName = `R`;
    const signatureArgumentGenericBaseName = `A`;
    let signatureArgs = [];

    for (let si = 1; si <= signatureArgumentCount; si++) {
        generics.push(`${signatureArgumentGenericBaseName}${si}`);
        signatureArgs.push(
            `${argumentBaseName}${si}: ${signatureArgumentGenericBaseName}${si}`,
        );
    }

    for (let fi = 1; fi <= composeArgCount; fi++) {
        const returnValue = `${returnValueGenericBaseName}${fi}`;
        const fnArgs =
            fi < composeArgCount
                ? [
                      `${argumentBaseName}: ${returnValueGenericBaseName}${fi +
                          1}`,
                  ]
                : signatureArgs;

        generics.push(returnValue);

        fns.push(
            `${fnBaseName}${fi}: (${fnArgs.join(", ")}) => ${returnValue}`,
        );
    }

    return `${indent}<${generics.join(", ")}>(${fns.join(
        ", ",
    )}): (${signatureArgs.join(", ")}) => ${returnValueGenericBaseName}1;`;
};

const createPipeOverload = (
    composeArgCount: number = 20,
    signatureArgumentCount: number = 5,
) => {
    const indent = `    `;
    const generics = [];
    const fns = [];
    const fnBaseName = `fn`;
    const argumentBaseName = `arg`;
    const returnValueGenericBaseName = `R`;
    const signatureArgumentGenericBaseName = `A`;
    let signatureArgs = [];

    for (let si = 1; si <= signatureArgumentCount; si++) {
        generics.push(`${signatureArgumentGenericBaseName}${si}`);
        signatureArgs.push(
            `${argumentBaseName}${si}: ${signatureArgumentGenericBaseName}${si}`,
        );
    }

    for (let fi = 1; fi <= composeArgCount; fi++) {
        const returnValue = `${returnValueGenericBaseName}${fi}`;
        const fnArgs =
            fi > 1
                ? [
                      `${argumentBaseName}: ${returnValueGenericBaseName}${fi -
                          1}`,
                  ]
                : signatureArgs;

        generics.push(returnValue);

        fns.push(
            `${fnBaseName}${fi}: (${fnArgs.join(", ")}) => ${returnValue}`,
        );
    }

    return `${indent}<${generics.join(", ")}>(${fns.join(
        ", ",
    )}): (${signatureArgs.join(
        ", ",
    )}) => ${returnValueGenericBaseName}${composeArgCount};`;
};

const createChainOverload = (composeArgCount: number = 20) => {
    const indent = `    `;
    const generics = [];
    const fns = [];
    const fnBaseName = `fn`;
    const argumentBaseName = `arg`;
    const returnValueGenericBaseName = `R`;
    const signatureGenerics = [`T`];
    let signatureArgs = [];

    signatureArgs.push(`value: T`);

    for (let fi = 1; fi <= composeArgCount; fi++) {
        const returnValue = `${returnValueGenericBaseName}${fi}`;
        const fnArgs =
            fi > 1
                ? [
                      `${argumentBaseName}: ${returnValueGenericBaseName}${fi -
                          1}`,
                  ]
                : signatureArgs;

        generics.push(returnValue);

        fns.push(
            `${fnBaseName}${fi}: (${fnArgs.join(", ")}) => ${returnValue}`,
        );
    }

    return `${indent}<${[...signatureGenerics, ...generics].join(
        ", ",
    )}>(${fns.join(", ")}): ${returnValueGenericBaseName}${composeArgCount};`;
};

const createArgumentsToArrayInterface = (argumentCount: number = 20) => {
    const overloads = [];

    for (let argCount = 1; argCount <= argumentCount; argCount++) {
        overloads.push(createArgumentsToArrayOverload(argCount));
    }

    return (
        `export interface ArgumentsToArray {\n` + overloads.join("\n") + `\n}`
    );
};

const createComposeInterface = (
    functionCount: number = 20,
    signatureArgumentCount = 5,
) => {
    const overloads = [];

    for (let funcCount = 1; funcCount <= functionCount; funcCount++) {
        for (
            let sigArgCount = 0;
            sigArgCount <= signatureArgumentCount;
            sigArgCount++
        ) {
            overloads.push(createComposeOverload(funcCount, sigArgCount));
        }
    }

    overloads.push(`    (...fns: Function[]): (...args: any[]) => any;`);

    return `export interface Compose {\n` + overloads.join("\n") + `\n}`;
};

const createPipeInterface = (
    functionCount: number = 20,
    signatureArgumentCount = 5,
) => {
    const overloads = [];

    for (let funcCount = 1; funcCount <= functionCount; funcCount++) {
        for (
            let sigArgCount = 0;
            sigArgCount <= signatureArgumentCount;
            sigArgCount++
        ) {
            overloads.push(createPipeOverload(funcCount, sigArgCount));
        }
    }

    overloads.push(`    (...fns: Function[]): (...args: any[]) => any;`);

    return `export interface Pipe {\n` + overloads.join("\n") + `\n}`;
};

const createChainInterface = (functionCount: number = 20) => {
    const overloads = [];

    for (let funcCount = 1; funcCount <= functionCount; funcCount++) {
        overloads.push(createChainOverload(funcCount));
    }

    //overloads.push(`    (...fns: Function[]): any;`);

    return `export interface Chain {\n` + overloads.join("\n") + `\n}`;
};

const pipeDst = join(__dirname, "../generated/pipe.ts");

mkdirp(dirname(pipeDst), err => {
    if (err)
        return console.error(
            `Failed to create pipe type definitions`,
            err.message,
        );

    writeFile(pipeDst, createPipeInterface(), "utf-8", err => {
        if (err)
            return console.error(
                `Failed to create pipe type definitions`,
                err.message,
            );

        console.log(`Successfully created compose reverse type definitions`);
    });
});

const chainDst = join(__dirname, "../generated/chain.ts");

mkdirp(dirname(chainDst), err => {
    if (err)
        return console.error(
            `Failed to create chain type definitions`,
            err.message,
        );

    writeFile(chainDst, createChainInterface(), "utf-8", err => {
        if (err)
            return console.error(
                `Failed to create chain type definitions`,
                err.message,
            );

        console.log(`Successfully created chain type definitions`);
    });
});

const composeDst = join(__dirname, "../generated/compose.ts");

mkdirp(dirname(composeDst), err => {
    if (err)
        return console.error(
            `Failed to create compose type definitions`,
            err.message,
        );

    writeFile(composeDst, createComposeInterface(), "utf-8", err => {
        if (err)
            return console.error(
                `Failed to create compose type definitions`,
                err.message,
            );

        console.log(`Successfully created compose type definitions`);
    });
});

const curryDst = join(__dirname, "../generated/curry.ts");

mkdirp(dirname(curryDst), err => {
    if (err)
        return console.error(
            `Failed to create curry type definitions`,
            err.message,
        );

    writeFile(curryDst, generateCurryTypes(10), "utf-8", err => {
        if (err)
            return console.error(
                `Failed to create curry type definitions`,
                err.message,
            );

        console.log(`Successfully created curry type definitions`);
    });
});

const argumentsToArrayDst = join(__dirname, "../generated/ArgumentsToArray.ts");

mkdirp(dirname(argumentsToArrayDst), err => {
    if (err)
        return console.error(
            `Failed to create argumentsToArray type definitions`,
            err.message,
        );

    writeFile(
        argumentsToArrayDst,
        createArgumentsToArrayInterface(),
        "utf-8",
        err => {
            if (err)
                return console.error(
                    `Failed to create argumentsToArray type definitions`,
                    err.message,
                );

            console.log(
                `Successfully created argumentsToArray type definitions`,
            );
        },
    );
});
